package cz.spst;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class HelloWorldTest {
	HelloWorld hello;

	@Before
	public void setUp() {
		hello = new HelloWorld();
	}

	@Test
	public void greet() {
		assertEquals("Hello World!", hello.greet("World"));
	}
}
