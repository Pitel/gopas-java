package cz.spst;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PointTest {
	public static final float DELTA = 0.00001f;

	@Test
	public void eq2Test() {
		assertEquals(new Point2D(1, 2), new Point2D(1, 2));
		assertNotEquals(new Point2D(1, 2), new Point2D(3, 4));
	}

	@Test
	public void eq3Test() {
		assertEquals(new Point3D(1, 2, 3), new Point3D(1, 2, 3));
		assertNotEquals(new Point3D(1, 2, 3), new Point3D(4, 5, 6));
	}

	@Test(expected = IllegalArgumentException.class)
	public void distance2Test() {
		assertEquals(0, Point2D.ZERO.distanceTo(Point2D.ZERO), DELTA);
		assertEquals(1, Point2D.ZERO.distanceTo(new Point2D(0, 1)), DELTA);
		assertEquals(Math.sqrt(2), Point2D.ZERO.distanceTo(new Point2D(1, 1)), DELTA);
		Point2D.ZERO.distanceTo(Point3D.ZERO);
	}

	@Test(expected = IllegalArgumentException.class)
	public void distance3Test() {
		assertEquals(0, Point3D.ZERO.distanceTo(Point3D.ZERO), DELTA);
		assertEquals(1, Point3D.ZERO.distanceTo(new Point3D(0, 0, 1)), DELTA);
		assertEquals(Math.sqrt(3), Point3D.ZERO.distanceTo(new Point3D(1, 1, 1)), DELTA);
		Point3D.ZERO.distanceTo(Point2D.ZERO);
	}
}
