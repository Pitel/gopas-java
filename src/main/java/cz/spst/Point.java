package cz.spst;

public interface Point {
	float distanceTo(final Point p);
}
