package cz.spst;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Point2D implements Point {
	public static final Point2D ZERO = new Point2D(0, 0);

	public final float x, y;

	public Point2D(final float x, final float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public float distanceTo(final Point p) {
		if (p.getClass() == getClass()) {
			final Point2D p_ = (Point2D) p;
			final float dx = Math.abs(x - p_.x);
			final float dy = Math.abs(y - p_.y);
			return (float) Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		}
		throw new IllegalArgumentException();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(x)
			.append(y)
			.toHashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		final Point2D p = (Point2D) obj;
		return new EqualsBuilder()
			.append(x, p.x)
			.append(y, p.y)
			.isEquals();
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
