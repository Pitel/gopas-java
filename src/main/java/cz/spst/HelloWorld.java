package cz.spst;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class HelloWorld {
	public static void main(final String[] args) {
		final HelloWorld hello = new HelloWorld();
		System.out.println(hello.greet("World"));

		final int[] x = {0};
		final ExecutorService pool = Executors.newCachedThreadPool();
		for (int i = 0; i < 100_000; i++) {
			pool.submit(() -> {
				x[0]++;
			});
			pool.submit(() -> {
				x[0]--;
			});
		}
		pool.shutdown();
		System.out.println(x[0]);
	}

	public String greet(final String who) {
		return "Hello " + who + "!";
	}
}
