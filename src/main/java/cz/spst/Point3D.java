package cz.spst;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Point3D extends Point2D {
	public static final Point3D ZERO = new Point3D(0, 0, 0);

	public final float z;

	public Point3D(final float x, final float y, final float z) {
		super(x, y);
		this.z = z;
	}

	@Override
	public float distanceTo(final Point p) {
		if (p.getClass() == getClass()) {
			final Point3D p_ = (Point3D) p;
			final float dx = Math.abs(x - p_.x);
			final float dy = Math.abs(y - p_.y);
			final float dz = Math.abs(z - p_.z);
			return (float) Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2));
		}
		throw new IllegalArgumentException();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.appendSuper(super.hashCode())
			.append(z)
			.toHashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		final Point3D p = (Point3D) obj;
		return new EqualsBuilder()
			.appendSuper(super.equals(obj))
			.append(z, p.z)
			.isEquals();
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ", " + z + ")";
	}
}
